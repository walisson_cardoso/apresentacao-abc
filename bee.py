"""
Indivíduo do Artificial Bee Colony

@author: Walisson
"""
import numpy as np;

class Bee:
    def __init__(self, evalFunc, dim, boundaries):
        # O construtor recebe a função de avaliação, número de dimensões do
        # problema e os limites mínimo e máximo do espaço de busca.
        self.evalFunc = evalFunc
        self.D = dim
        self.lb = boundaries[0]
        self.ub = boundaries[1]
        # Inicializa posição da abelha, valores de fitness e valor da função 
        # objetivo na posição da abelha.
        # Trial é inicializado com zero.
        self.x = np.zeros(dim)
        self.fitness = float("-inf")
        self.f = float("inf")
        self.trial = 0
    
    
    def init(self):
        # Inicia a posição de uma abelha e calcula seu fitness (ou reinicia a
        # posição caso esta seja uma scout).
        self.x = np.random.uniform(self.lb, self.ub, self.D);
        self.f = self.evalFunc(self.x, self.D)
        self.fitness = self.calcFitness(self.f)
        self.trial = 0;

    
    def perturbation(self, refX):
        # Modifica a posição da abelha conforme a posição de referência 
        # recebida (refX).
        # Seleciona aleatoriamente uma coordenada para ser modificada, realiza
        # o deslocamento e atualiza a posição apenas se o fitness foi melhorado.
        newX = np.copy(self.x)
        pc = np.random.randint(self.D)
        shift = (np.random.rand()-0.5)*2 #[-1,+1]
        
        newX[pc] = newX[pc] + (newX[pc]-refX[pc])*shift
        newX[pc] = np.clip(newX[pc], self.lb, self.ub)
        
        newF = self.evalFunc(newX, self.D)
        newFitness = self.calcFitness(newF)
        # Atualiza posição se fitness melhorou
        if newFitness > self.fitness:
            self.trial = 0
            self.x = np.copy(newX)
            self.f = newF
            self.fitness = newFitness
        else:
            self.trial += 1
            
    
    def calcFitness(self, f):
        # Cálculo do fitness é diretamente baseado na função objetivo
        # Não existem fitnesses negativos. Isso é feito para a seleção por
        # roleta na fase de onlooker bees.
        fitness = 0.0
        if f >= 0:
            fitness = 1.0/(f+1)
        else:
            fitness = 1.0 + abs(f)
        
        return fitness;