from artificialBeeColony import ArtificialBeeColony;
import matplotlib.pyplot as plt
import numpy as np;

def executa_abc(popSize, iterations, costFunction, boundaries, dimensionality):
    
    div = (boundaries[1] - boundaries[0])/100
    r = np.arange(boundaries[0] - 10*div, boundaries[1] + 10*div, div)
    xx, yy = np.meshgrid(r, r)
    zz = np.copy(xx)
    for i in range(xx.shape[0]):
        zz[i, :] = [costFunction([xx[i, j], yy[i, j]], dimensionality) for j in range(xx.shape[1])]
        
    limit = (popSize // 2) * dimensionality
    
    # Cria objeto ABC
    abc = ArtificialBeeColony(popSize, limit, costFunction, dimensionality, boundaries)
    
    # Arrays para guardar estatísticas
    convergence_curve = np.zeros(iterations)
    mean_value = np.zeros(iterations)
    std_value = np.zeros(iterations)
    last_pos = np.array([])
    
    # Iterações
    for i in range(iterations):
        convergence_curve[i] = abc.globalMin
        mean_value[i], std_value[i] = abc.swarm_stat()
        abc.iterate()
        trial = np.array([abc.bees[j].trial for j in range(abc.SN)])
        
        # Salva figura com posicionamento da população
        fig = plt.figure(figsize=(8,6))
        pos = np.matrix([[abc.bees[j].x[0], abc.bees[j].x[1]] for j in range(abc.SN)])
        plt.contour(xx, yy, zz)
        
        if last_pos.size != 0:
            for j in range(abc.SN):
                plt.plot([last_pos[j,0], pos[j,0]], [last_pos[j,1], pos[j,1]], color='b', linewidth=3)
        last_pos = np.copy(pos)
        
        for j in range(abc.SN):
            if trial[j] > 1:
                plt.plot(pos[j,0], pos[j,1], 'ro', markersize=10)
            else:
                plt.plot(pos[j,0], pos[j,1], 'go', markersize=10)
        plt.savefig("plots/plot" + str(i) + ".png")
        plt.close(fig)
        
    # Mostra gráfico de convergência
    x = range(1, iterations+1)
    plt.figure(figsize=(12,8))
    plt.plot(x, mean_value)
    plt.fill_between(x, mean_value+std_value, mean_value-std_value, facecolor='blue', alpha=0.1)
    plt.plot(x, convergence_curve)
    
    plt.title("Execução do ABC", fontsize=18)
    plt.xlabel("iteração", fontsize=14)
    plt.ylabel("função objetivo", fontsize=14)
    plt.legend(('valor médio','melhor encontrado'), fontsize=13)
    plt.show()
    
    # Mostra melhor solução encontrada
    print("Melhor solução encontrada:")
    print("f(%.6f , %.6f) = %.10f" % (abc.globalPos[0], abc.globalPos[1], abc.globalMin))