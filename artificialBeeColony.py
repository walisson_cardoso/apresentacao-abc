"""
Implementação do algoritmo Artificial Bee Colony (ABC).

@author: Walisson
"""
from bee import Bee;
import numpy as np;

class ArtificialBeeColony:
    def __init__(self, NP, limit, evalFunc, dim, boundaries):
        # O construtor recebe o número de abelhas do enxame, o número máximo
        # de tentativas (trial), a função de avaliação, o número de dimensões
        # do problema e os limites máximo e mínimo do espaço de busca.
        self.NP = NP
        self.SN = NP // 2
        self.limit = limit;
        self.evalFunc = evalFunc
        # Mínimo encontrado e sua localização
        self.globalMin = float("inf")
        self.globalPos = np.zeros(dim)
        # Inicia abelhas empregadas
        self.bees = [];
        for i in range(self.SN):
            self.bees.append(Bee(evalFunc, dim, boundaries))
            self.bees[i].init()
            
    
    def iterate(self):
        # Realiza as fases do algoritmo
        self.sendEmployedBees()
        self.sendOnLookerBees()
        self.sendScoutBees()
        self.memorizeBestSource()
    
    
    def sendEmployedBees(self):
        # Causa perturbações nas posições das abelhas empregadas, tentando
        # melhorá-las.
        for i in range(self.SN):
            self.updateSolution(i)
    
    
    def sendOnLookerBees(self):
        # Abelhas observadoras são enviadas proporcionalmente ao fitness de uma
        # das soluções atuais (abelhas empregadas). Esta função realiza os
        # passos: 1_ calcular probabilidade, 2_ enviar observadoras
        
        # Calcula probabilidade das soluções serem preferidas pelas observadoras
        maxFit = self.bees[0].fitness;
        for bee in self.bees:
            if bee.fitness > maxFit:
                maxFit = bee.fitness
        
        prob = np.zeros(self.SN)
        for i in range(self.SN):
            fit = self.bees[i].fitness
            prob[i] = (0.9*(fit/maxFit))+0.1
        
        # Envia abelhas observadoras para as soluções
        i = 0
        t = 0
        while t < self.SN:
            if np.random.rand() < prob[i]:
                t += 1
                self.updateSolution(i)
            i += 1
            i = i % self.SN
    
    
    def sendScoutBees(self):
        # Primeiramente seleciona a abelhas que teve mais tentativas sem
        # sucesso do melhoramento de sua solução.
        # Depois transforma esta abelha em uma scout. Teremos no máximo uma
        # scout por iteração.
        maxTrialIndex = 0;
        for i in range(self.SN):
            if self.bees[i].trial > self.bees[maxTrialIndex].trial:
                maxTrialIndex = i
        
        if self.bees[maxTrialIndex].trial >= self.limit:
            self.bees[maxTrialIndex].init()
    
    
    def memorizeBestSource(self):
        # Memoriza melhor solução encontrada
        for bee in self.bees:
            if bee.f < self.globalMin:
                self.globalMin = bee.f
                self.globalPos = np.copy(bee.x)
    
    
    def updateSolution(self, i):
        # Realiza a perturbação da posiçãod a abelha i
        neighbor = np.random.randint(self.SN)
        while self.SN > 1 and neighbor == i:
            neighbor = np.random.randint(self.SN)
        
        self.bees[i].perturbation(self.bees[neighbor].x)
        
    def swarm_stat(self):
        fit = np.zeros(self.SN)
        for i in range(self.SN):
            fit[i] = self.bees[i].f
        
        return (fit.mean(), fit.std())